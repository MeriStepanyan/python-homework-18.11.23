# 1.Implement a program to find the sum of the main diagonal elements of a matrix.
matrix=[[1,2,3],[4,5,6],[7,8,9]]
sum_m=0
for i in range(len(matrix)):
    for j in range(len(matrix[0])):
        if i==j:
            sum_m+=matrix[i][j]
print("The sum of the main diagonal elements is: ",sum_m)

# 2. Write a program that finds the maximum element in a matrix and its position (row and column).

matrix=[[1,2,3],[4,5,6],[7,8,9]]
max_m=matrix[0][0]

for i in range(len(matrix)):
    for j in range(len(matrix[0])):
        if matrix[i][j]>max_m:
            max_m=matrix[i][j]
            ind_i=i
            ind_j=j

print("The max element in a matrix is: ",max_m, " at position ",[ind_i],[ind_j])

# 3. Write a program to add two matrixes of the same size.

X=[[1,2,3],[4,5,6],[7,8,9]]
Y=[[10,20,30],[40,50,60],[70,80,90]]
Res=[[0,0,0],[0,0,0],[0,0,0]]
for i in range(len(X)):
    for j in range(len(Y)):
        Res[i][j]=X[i][j]+Y[i][j]
print("The result matrix: ",Res)

# 4. Write a Python program that prints 'True' if the given number is a double palindromic number and 'False' otherwise. 
# A double palindromic number is a number that is a palindrome in both its decimal and binary representations.
def int_to_binary(n):
    st=""
    while(n>0):
        st+=str(n % 2)
        n=n//2
    return st[::-1]
number=33

print(f"The binary representation of {number}: ", int_to_binary(number))   

def is_dec_palind(number):
    number=str(number)
    check=0
    for i in range(len(number)//2):
        if(number[i]==number[-1-i]):
            check+=1
    if(check==len(number)//2):
        return True  
    else:
        return False    

is_dec_palind=is_dec_palind(number)

bin_num=str(int_to_binary(number))
def is_bin_palind(bin_num):
    bin_num
    check=0
    for i in range(len(bin_num)//2):
        if(bin_num[i]==bin_num[-1-i]):
            check+=1
    if(check==len(bin_num)//2):
        return True  
    else:
        return False 
is_bin_palind=is_bin_palind(int_to_binary(number))    


def is_double_palindromic(bin_palind,dec_palind):
    if(is_dec_palind==is_bin_palind):
        print(f"Is {number} a double palindromic number? : True")
    else: 
        print(f"Is {number} a double palindromic number? : False")
is_double_palindromic(is_bin_palind,is_dec_palind)


# 5. Create a program to find and print the unique elements in a list.
list1 = [15, 20, 15, 30, 40, 40]     # Version 1
unique_list=[]
for i in list1:
    if list1.count(i)==1:
        unique_list.append(i)
print("The new list with unique elements:",unique_list)  

list2 = [10, 20, 10, 30, 40, 40]     # Version 2
unique_list_2 = list(dict.fromkeys(list2))
print("The new list with unique elements:",unique_list_2,sep="\n")

# 6. Write a Python program that takes an integer as input and prints its binary representation.
def int_to_binary(n):
 
    if (n > 1):
        int_to_binary(n >> 1)
    print(n & 1, end="")
q=5 
print(f"The binary representation of {q}: ")   
int_to_binary(q)
print('\n')

# 7. Implement a program to count the number of set bits (1s) in an integer.

def  countSetBits(n):
    count = 0
    while (n):
        count += n & 1
        n >>= 1
    return count
g=5
print(F"The number of set bits in {g}  is ->",countSetBits(g))

# 8. Write a program that swaps two numbers using bitwise operators.
def swap(s,t):
    s=s^t
    t=t^s
    s=s^t
    return s,t
s=10
t=20
print("Before swap: (S,T)=",s,t, " After swap (S,T)=",swap(s,t))

# 10. Implement a program to convert a binary number to its decimal equivalent.
def binary_to_int(binary):
    w=len(binary)-1
    bin_=reversed(binary)
    bin_to_int=0
    for h in bin_:
        bin_to_int+=int(h)*pow(2,w)
        w=w-1
    return bin_to_int    
binary='0110'
print(f"Binary {binary} to int :",binary_to_int(binary))   

# 11. Create a program to check if two words are anagrams of each other. An anagram is a word or 
# phrase formed by rearranging the letters of another word or phrase, using all the original letters exactly once.

def are_anagrams(str1, str2):
    if(len(str1)!=len(str2)):
        return False
    for i in range(len(str1)):
        if(str1[i] in str2):
            continue
        else:
            return False
    return True
str1="spam"
str2="maps"
print(f"Are '{str1}' and '{str2}' anagrams ?: ",are_anagrams(str1, str2))        


